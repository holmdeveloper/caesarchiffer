import { useState } from "react";
import styles from "../styles/Home.module.css";

export default function Home() {
  const [inputText, setInputText] = useState("");
  const [shift, setShift] = useState(0);
  const [outputText, setOutputText] = useState("");

  const handleInputChange = (e) => {
    setInputText(e.target.value);
  };

  const handleShiftChange = (e) => {
    setShift(parseInt(e.target.value, 10));
  };

  const solveCipher = () => {
    const result = shiftText(inputText, shift);
    setOutputText(result);
  };

  const shiftText = (text, shift) => {
    return text
      .split("")
      .map((char) => {
        if (char.match(/[a-z]/i)) {
          const code = char.charCodeAt(0);
          const isUpperCase = char === char.toUpperCase();
          const shiftedCode =
            ((((code - (isUpperCase ? 65 : 97) + shift) % 26) + 26) % 26) +
            (isUpperCase ? 65 : 97);
          return String.fromCharCode(shiftedCode);
        } else {
          return char;
        }
      })
      .join("");
  };

  const resetFields = () => {
    setInputText("");
    setShift(0);
    setOutputText("");
  };

  return (
    <div className={styles.container}>
      <div className={styles.labelContainer}>
        <label>
          Input Text:
          <input
            className={styles.inputField}
            type="text"
            value={inputText}
            onChange={handleInputChange}
          />
        </label>
      </div>
      <div className={styles.labelContainer}>
        <label>
          Shift:
          <input
            className={styles.inputField}
            type="number"
            value={shift}
            onChange={handleShiftChange}
          />
        </label>
      </div>
      <div className={styles.buttonContainer}>
        <button className={styles.solveButton} onClick={solveCipher}>
          Solve and Create
        </button>
        <button className={styles.resetButton} onClick={resetFields}>
          Reset
        </button>
      </div>
      <div className={styles.labelContainer}>
        <label className={styles.output}>
          Output Text:
          <div>{outputText}</div>
        </label>
      </div>
    </div>
  );
}
